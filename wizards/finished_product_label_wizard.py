# -*- coding: utf-8 -*-
from odoo import models, fields
from odoo.exceptions import ValidationError


class ProductionLabelWizard(models.TransientModel):
    _name = "etm_production_label.finished_product_label_wizard"
    _description = "Wizard to print id label of an item finished in production"

    product_quantity = fields.Integer()

    def _default_production(self):
        return self.env["mrp.production"].browse(self._context.get("active_id"))

    production_id = fields.Many2one(
        comodel_name="mrp.production",
        default=_default_production,
    )

    def print_finished_product_label(self):
        if self.production_id.state not in ("progress", "done"):
            raise ValidationError(
                "Solo se pueden generar las etiquetas de los productos en proceso o terminadas."
            )
        return self.env.ref("etm_production_label.finished_product_label").report_action(self)

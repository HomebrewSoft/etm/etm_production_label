from odoo import api, fields, models


class ProductionCertificate(models.TransientModel):
    _name = "mrp.production.certificate"
    _description = "MRP Production Certificate"

    production_id = fields.Many2one(
        comodel_name="mrp.production",
        required=True,
        readonly=True,
        default=lambda self: self.env["mrp.production"].browse(self._context["active_id"]),
    )
    user_id = fields.Many2one(
        comodel_name="res.users",
        default=lambda self: self.env.user,
        required=True,
        readonly=True,
    )
    lot_id = fields.Many2one(
        comodel_name="stock.production.lot",
        required=True,
    )

    @api.onchange("production_id")
    def _domain_lot_id(self):
        if not self.production_id:
            return
        return {
            "domain": {
                "lot_id": [
                    ("id", "in", self.production_id.mapped("finished_move_line_ids.lot_id").ids)
                ]
            }
        }

    def print(self):
        return self.env.ref("etm_production_label.mrp_production_certificate").report_action(self)

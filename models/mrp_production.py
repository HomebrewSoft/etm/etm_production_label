from odoo import api, fields, models


class Production(models.Model):
    _inherit = "mrp.production"

    last_digits_name = fields.Char(
        compute="_compute_last_digits_name",
    )

    @api.depends("name")
    def _compute_last_digits_name(self):
        for production in self:
            production.last_digits_name = production.name.split("/")[-1]

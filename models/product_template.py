# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = "product.product"

    pantones = fields.Char()
    barcode_read = fields.Char()
    print_quality = fields.Char()
    tack = fields.Char()
    observations = fields.Text()

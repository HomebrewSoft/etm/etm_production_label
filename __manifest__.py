# -*- coding: utf-8 -*-
{
    'name': 'ETM Production Label',
    'version': '13.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        "plan_production_order",
        "product_pricelist_package",
        "product_prototype",
        "purchase_stock",
        "sale_order_type",
        "stock",
    ],
    "data": [
        # security
        # data
        "data/format_product_label.xml",
        "data/format_production_id_label.xml",
        "data/format_delivery_label.xml",
        "data/format_transfers_label.xml",
        # reports
        "reports/finished_product_label.xml",
        "reports/mrp_production_certificate.xml",
        "reports/production_id_label.xml",
        "reports/delivery_label.xml",
        "reports/transfers_label.xml",
        "reports/production_order_2.xml",
        # wizards views
        "views/finished_product_label_wizard_form_view.xml",
        "views/production_label_wizard_form_view.xml",
        # views
        "views/mrp_production.xml",
        "views/stock_picking.xml",
        "views/format_transfer_label.xml",
        "views/mrp_production_certificate.xml",
        "views/product_template.xml",
    ],
    "css": ["static/src/css/production_order.css"],
}
